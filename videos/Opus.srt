﻿1
00:00:14,015 --> 00:00:22,920
Introdução:
Começa com uma dissonância desconcertante

2
00:00:23,655 --> 00:00:30,530
Depois do susto, um recitativo introdutório

3
00:00:42,240 --> 00:00:49,515
A dissonância inicial é repetida

4
00:01:04,631 --> 00:01:16,700
O tema inicial de duas notas do primeiro movimento é reapresentado

5
00:01:17,897 --> 00:01:26,050
Os temas dos movimentos anteriores são reapresentados

6
00:02:06,682 --> 00:02:17,733
Parte 1: Os instrumentos musicais
O prenúncio de um novo tema

7
00:02:45,822 --> 00:02:51,435
O novo tema é anunciado novamente
com mais ênfase

8
00:03:18,032 --> 00:03:20,559
Termina o recitativo introdutório

9
00:03:21,326 --> 00:03:33,876
Os violoncelos introduzem o tema da Alegria

10
00:04:02,908 --> 00:04:16,232
Ov violinos e o fagote entoam o tema da Alegria

11
00:04:45,679 --> 00:05:01,461
O naipe de cordas faz coro ao tema da Alegria

12
00:05:27,288 --> 00:05:34,260
Toda a orquestra é tomada pelo tema

13
00:06:37,294 --> 00:06:43,661
Tudo encaminha-se para um final harmônico

14
00:06:44,962 --> 00:06:48,472
Só que ainda falta alguma coisa

15
00:06:48,804 --> 00:06:56,548
Volta o desconcerto inicial

16
00:06:56,004 --> 00:07:09,554
Parte 2: A Voz Humana
Ó amigos, não estes sons!

17
00:07:15,560 --> 00:07:29,610
Ao invés, cantemos algo mais agradável

18
00:07:31,483 --> 00:07:46,132
E cheio de alegria!

19
00:07:49,207 --> 00:07:53,464
Alegre! Alegre!

20
00:07:53,607 --> 00:07:58,511
Alegria, formosa centelha divina
Filha de Elíseo

21
00:07:58,811 --> 00:08:04,510
Ébrios de foto entramos
Em teu santuário celeste!

22
00:08:04,610 --> 00:08:10,514
Tua magia volta a unir
O que o costume rigorosamente dividiu

23
00:08:11,154 --> 00:08:16,397
Todos os homens são irmãos
Ali onde teu doce vôo se detém.

24
00:08:16,497 --> 00:08:22,424
Tua magia volta a unir
O que o costume rigorosamente dividiu

25
00:08:23,533 --> 00:08:29,172
Todos os homens são irmãos
Ali onde te doce vôo se detém

26
00:08:36,046 --> 00:08:41,550
Quem já conquistou o maior tesouro
De ser o amigo de um amigo

27
00:08:41,979 --> 00:08:47,360
Quem já conquistou uma mulher amável
Alegre-se conosco!

28
00:08:47,759 --> 00:08:53,028
Sim, mesmo se for apenas uma alma,
Uma única alma em todo o mundo.

29
00:08:53,622 --> 00:08:59,303
Mas aquele que falhou nisso
Que fique chorando sozinho!

30
00:09:00,168 --> 00:09:05,512
Sim, mesmo se for apenas uma alma,
Uma única alma em todo o mundo.

31
00:09:05,984 --> 00:09:11,983
Mas aquele que falhou nisso
Que fique chorando sozinho

32
00:09:18,525 --> 00:09:24,055
Alegria bebem todos os seres
No seio da Natureza:

33
00:09:25,053 --> 00:09:30,390
Todos os bons, todos os maus,
Seguem seu rastro de rosas

34
00:09:30,838 --> 00:09:36,432
Ela nos deu beijos e vinho
E um amigo leal até a morte;

35
00:09:36,999 --> 00:09:42,949
Deu força para a vida aos mais humildes
E ao anjo que se ergue diante de Deus!

36
00:09:43,577 --> 00:09:55,156
Ela nos deu beijos e vinho
E um amigo leal até a morte;

37
00:09:56,196 --> 00:10:01,134
Deu força para a vida aos mais humildes
E ao anjo que se ergue diante de Deus!

38
00:10:01,769 --> 00:10:05,202
Se ergue diante de Deus!

39
00:10:05,772 --> 00:10:16,046
De Deus!
De Deus!

40
00:10:19,106 --> 00:10:31,828
Parte 3: O Criador
O tema do Criador é anunciado

41
00:11:02,583 --> 00:11:10,661
Alegremente, alegremente como seus sóis
Como seus sóis voem

42
00:11:10,671 --> 00:11:17,775
Alegremente, como seus sóis voem
Através do esplêndido espaço celeste

43
00:11:17,875 --> 00:11:26,386
Se expressem, irmãos, em seus caminhos!
Se expressem, irmãos, em seus caminhos!

44
00:11:26,778 --> 00:11:34,627
Alegremente como um herói diante da vitória.
Como um herói diante da vitória.

45
00:11:35,095 --> 00:11:42,469
Se expressem irmãos, em seus caminhos!
Se expressem irmãos, em seus caminhos!

46
00:11:42,959 --> 00:11:59,184
Alegremente como um herói diante da vitória.

47
00:13:31,711 --> 00:13:44,317
A resposta se aproxima.

48
00:13:48,960 --> 00:13:56,070
Parte 4: Todos juntos
Alegria, formosa centelha divina
Filha do Elíseo

49
00:13:56,056 --> 00:14:03,398
Ébrios de fogo entramos
Em teu santuário celeste!

50
00:14:03,498 --> 00:14:10,883
Tua magia volta a unir
O que o costume rigorosamente dividiu

51
00:14:10,983 --> 00:14:18,513
Todos os homens são irmãos
Ali onde o teu doce vôo se detém.

52
00:14:18,613 --> 00:14:26,533
Tua magia volta a unir
O que o costume rigorosamente dividiu

53
00:14:26,633 --> 00:14:33,865
Todos os homens são irmãos
Ali onde teu doce vôo se detém.

54
00:14:41,447 --> 00:15:06,166
Coro masculino canta o tema do Criador:
Abracem-se milhões!
Enviem este beijo para todo o mundo!

55
00:15:08,908 --> 00:15:33,821
Canto feminino canta o tema do Criador:
Abracem-se milhões!
Enviem este beijo para todo o mundo!

56
00:15:33,921 --> 00:15:59,592
Coro masculino:
Irmãos, além do céu estrelado
Mora um Pai Amado

57
00:16:01,117 --> 00:16:24,450
Coro feminino:
Irmãos, além do céu estrelado
Mora um Pai Amado

58
00:16:45,540 --> 00:17:01,946
Coro completo:
Milhões de deprimem diante Dele?

59
00:17:02,989 --> 00:17:18,602
Mundo, você percebe seu Criador?

60
00:17:20,406 --> 00:17:34,285
Procure-o mais acima do Céu estrelado!

61
00:17:35,755 --> 00:17:48,688
Sobre as estrelas onde Ele mora!

62
00:18:03,205 --> 00:18:22,201
Procure-o mais acima do Céu estrelado!
Sobre as estrelas onde Ele mora!

63
00:18:22,965 --> 00:18:40,215
(As vozes se misturam repetindo a letra)
(Os instrumentos se misturam entoando temas)
(As estrofes cantadas se repetem)

64
00:22:49,873 --> 00:22:57,776
A resolução do desconcerto do início

65
00:23:01,476 --> 00:23:11,144
Todos juntos

66
00:23:20,589 --> 00:23:33,011
e alegres!

